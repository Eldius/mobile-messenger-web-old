package net.eldiosantos.messenger;

import javax.naming.NamingException;
import javax.servlet.http.*;

import org.eclipse.jetty.plus.jndi.Resource;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.h2.jdbcx.JdbcDataSource;

import java.io.File;
import java.io.FilenameFilter;

public class Main extends HttpServlet {

  public static void main( String[] args ) throws Exception
  {
        new Main().start();
  }

    public void start() throws Exception {
        this.up();
    }

    private void up() throws Exception {
        String jetty_home = System.getProperty("jetty.home", ".");

        Server server = createServer();

        setupServer(server);

        WebAppContext webapp = setupWebappContext();
        server.setHandler(webapp);

        server.start();
        server.join();
    }

    private void setupServer(Server server) throws NamingException {
        Configuration.ClassList classlist = Configuration.ClassList.setServerDefault(server);
        classlist.addBefore(
                "org.eclipse.jetty.webapp.JettyWebXmlConfiguration",
                "org.eclipse.jetty.annotations.AnnotationConfiguration",
                "org.eclipse.jetty.webapp.FragmentConfiguration",
                "org.eclipse.jetty.plus.webapp.EnvConfiguration",
                "org.eclipse.jetty.plus.webapp.PlusConfiguration"
        );
        setupCdiContext(server);
        setupDatasource(server);
    }

    private void setupDatasource(Server server) throws NamingException {
        final JdbcDataSource datasourceDef = new JdbcDataSource();
        datasourceDef.setUrl("jdbc:h2:./target/running.db");
        datasourceDef.setUser("user");
        datasourceDef.setPassword("pass");
        final Resource datasourceBean = new Resource("jdbc/datasource", datasourceDef);
        server.addBean(datasourceBean);
    }

    private void setupCdiContext(Server server) throws NamingException {
        final Resource cdiContext = new Resource("javax.enterprise.inject.spi.BeanManager", "org.jboss.weld.resources.ManagerObjectFactory", null);
        server.addBean(cdiContext);
    }

    private WebAppContext setupWebappContext() {
        // The WebAppContext is the entity that controls the environment in
        // which a web application lives and breathes. In this example the
        // context path is being set to "/" so it is suitable for serving root
        // context requests and then we see it setting the location of the war.
        // A whole host of other configurations are available, ranging from
        // configuring to support annotation scanning in the webapp (through
        // PlusConfiguration) to choosing where the webapp will unpack itself.
        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/");
        File warFile = getWarFile();

        webapp.setWar(warFile.getAbsolutePath());
        webapp.addServerClass("-org.eclipse.jetty.servlet.ServletContextHandler.Decorator");
        webapp.setAttribute(
                "org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",
                ".*/[^/]*servlet-api-[^/]*\\.jar$|.*/javax.servlet.jsp.jstl-.*\\.jar$|.*/[^/]*taglibs.*\\.jar$" );

        return webapp;
    }

    private File getWarFile() {
        return new File("messenger-heroku-app/target/dependency").listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".war");
                }
            })[0];
    }

    private Server createServer() {
        // Create a basic jetty server object that will listen on port 8080.
        // Note that if you set this to port 0 then a randomly available port
        // will be assigned that you can either look in the logs for the port,
        // or programmatically obtain it for use in test cases.
        return new Server(Integer.valueOf(System.getenv("PORT")));
    }
}